<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.heed.openapps</groupId>
        <artifactId>service</artifactId>
        <version>1.0.0</version>
    </parent>

    <groupId>org.heed.openapps</groupId>
    <artifactId>org.heed.openapps.entity.service</artifactId>
    <version>1.0.0</version>
    <packaging>bundle</packaging>

    <name>OpenApps Entity Service Bundle</name>
    <description>OpenApps Entity Service Bundle</description>
	
	<pluginRepositories>
  		<pluginRepository>
    		<id>eclipse.virgo.build.bundles.release</id>
    		<name>Eclipse Virgo Build</name>
    		<url>http://build.eclipse.org/rt/virgo/maven/bundles/release</url>
  		</pluginRepository>
  		<pluginRepository>
    		<id>com.springsource.repository.bundles.external</id>
    		<name>SpringSource Enterprise Bundle Repository - External Bundle Releases</name>
    		<url>http://repository.springsource.com/maven/bundles/external</url>
  		</pluginRepository>
	</pluginRepositories>
	
    <dependencies>
        <dependency>
            <groupId>org.osgi</groupId>
            <artifactId>org.osgi.core</artifactId>
            <version>4.2.0</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>servlet-api</artifactId>
			<version>2.5</version>
	   	</dependency>
        <dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-beans</artifactId>
			<version>3.1.0.RELEASE</version>
		</dependency>
        <dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-context</artifactId>
			<version>3.1.0.RELEASE</version>
		</dependency>
		<dependency>
			<groupId>commons-lang</groupId>
			<artifactId>commons-lang</artifactId>
			<version>2.6</version>
		</dependency>            
		<dependency>
			<groupId>org.apache.shiro</groupId>
			<artifactId>shiro-all</artifactId>
			<version>1.2.2</version>
		</dependency>    
		<dependency>
			<groupId>org.apache.lucene</groupId>
			<artifactId>lucene-core</artifactId>
			<version>3.5.0</version>
		</dependency>
		<dependency>
			<groupId>org.heed.openapps</groupId>
			<artifactId>org.heed.openapps.core.api</artifactId>
			<version>1.0.0</version>
		</dependency>
		<dependency>
			<groupId>org.heed.openapps</groupId>
			<artifactId>org.heed.openapps.node.api</artifactId>
			<version>1.0.0</version>
		</dependency>
		<dependency>
			<groupId>org.heed.openapps</groupId>
			<artifactId>org.heed.openapps.entity.api</artifactId>
			<version>1.0.0</version>
		</dependency>
		<dependency>
			<groupId>org.heed.openapps</groupId>
			<artifactId>org.heed.openapps.scheduling.api</artifactId>
			<version>1.0.0</version>
		</dependency>
		<dependency>
			<groupId>org.heed.openapps</groupId>
			<artifactId>org.heed.openapps.cache.api</artifactId>
			<version>1.0.0</version>
		</dependency>
		
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.felix</groupId>
                <artifactId>maven-bundle-plugin</artifactId>
                <version>2.3.7</version>
                <extensions>true</extensions>
                <configuration>
                    <instructions>
                        <Bundle-SymbolicName>${project.artifactId}</Bundle-SymbolicName>
                        <Bundle-Version>${project.version}</Bundle-Version>
                        <!-- Bundle-ClassPath>.,lib/neo4j-rest-graphdb-1.8-SNAPSHOT.jar</Bundle-ClassPath-->
                        <Export-Package>
                        	org.heed.openapps.entity.service;version=${project.version},
                        	org.heed.openapps.dictionary.service;version=${project.version}
                        </Export-Package>
                        <Import-Package>
                           *
                        </Import-Package>
                    </instructions>
                </configuration>
            </plugin>
    		<plugin>
      		<groupId>org.apache.maven.plugins</groupId>
      		<artifactId>maven-jar-plugin</artifactId>
      		<version>2.4</version>
      		<configuration>
        			<archive>
          				<manifestFile>
            				target/classes/META-INF/MANIFEST.MF
          				</manifestFile>
        			</archive>
      		</configuration>
    		</plugin>
        </plugins>
    </build>
    
    <repositories>
    	<repository>
      		<id>neo4j-public-repository</id>
      		<url>http://m2.neo4j.org</url>
    	</repository>
    </repositories>	
</project>
